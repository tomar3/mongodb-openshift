FROM mongo:4.4.5
RUN chgrp -R 0 /data && \
    chmod -R g=u /data
